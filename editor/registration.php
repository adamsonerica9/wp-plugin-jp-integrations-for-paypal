<?php
namespace JakeParis\PayPayIntegrations;

function enqueue_public_styles(){
	wp_enqueue_style('jp-paypal/public',
		JP_PAYPAL_PLUGIN_URL . 'editor/build/style-index.css',
		[
		],
		JP_PAYPAL_PLUGIN_VERSION
	);
}

add_action('init', function(){

	register_block_type( 'jp-paypal/pay-now-button', [
		'render_callback' => function($atts,$content) {
			enqueue_public_styles();
			return $content;
		}
	]);

});

add_action('enqueue_block_editor_assets', function(){

	enqueue_public_styles();

	wp_enqueue_style('jp-paypal/admin',
		JP_PAYPAL_PLUGIN_URL . 'editor/build/index.css',
		[
		],
		JP_PAYPAL_PLUGIN_VERSION
	);

	wp_enqueue_script('jp-paypal/admin',
		JP_PAYPAL_PLUGIN_URL . 'editor/build/index.js',
		[
			'wp-element',
			'wp-blocks',
			'wp-editor',
			'wp-components',
			'wp-element',
			// 'lodash',
		],
		JP_PAYPAL_PLUGIN_VERSION
	);

	$blockSettings = [];
	$allSettings = JP_Settings::getSettings();
	$blockSettings = json_encode([
		'default_tax_rate' => $allSettings['default_tax_rate'],
		'merchant_id' => $allSettings['merchant_id'],
		'default_success_page' => get_permalink( $allSettings['default_success_page'] ),
		'default_cancel_page' => get_permalink( $allSettings['default_cancel_page'] ),
	]);
	$js = <<<JS
	var jakeparis = window.jakeparis || {};
	jakeparis.paypal = {$blockSettings};
JS;

	wp_add_inline_script( 'jp-paypal/admin', $js, 'before' );

});
