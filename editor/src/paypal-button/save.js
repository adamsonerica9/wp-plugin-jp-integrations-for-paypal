
import {
	getButtonImage,
	makePayLink,
} from './lib';

import classnames from 'classnames';

import {
	useBlockProps,
} from '@wordpress/block-editor';

import {
	Fragment,
} from '@wordpress/element';

import {
	RichText,
} from '@wordpress/editor';


export default function save({attributes}) {
	
	const blockProps = useBlockProps.save({});

	const imageUrl = getButtonImage( attributes );

	const customButtonClasses = classnames({
		'jp-custom-paypal-button': true,
		'wp-block-button__link': true,
		'_with-logo': (attributes.include_logo===true),
	});

	const submitButton = (attributes.button_type==='custom') ? (
		<div className="wp-block-button">
			<RichText.Content
				tagName="a"
				className={ customButtonClasses }
				value={ attributes.button_text }
				onclick={ (attributes.transport_type==='link') ? false : "this.closest('form').submit()" }
				href={ (attributes.transport_type==='link') ? makePayLink(attributes) : "#" }
			/>
		</div>
	) : (
		<input type="image" src={ imageUrl } name="submit" alt="Pay with Paypal" />
	);

	if( attributes.transport_type === 'link' )
		return submitButton;

	return (
		<form {...blockProps}  action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top">
			<input type="hidden" name="cmd" value="_xclick" />
			<input type="hidden" name="business" value={ attributes.merchant_id } />
			<input type="hidden" name="lc" value="US" />
			<input type="hidden" name="amount" value={ attributes.amount } />
			<input type="hidden" name="currency_code" value="USD" />
			<input type="hidden" name="button_subtype" value="services" />
			<input type="hidden" name="rm" value="1" />

			{ attributes.allow_special_instructions && (
				<Fragment>
					<input type="hidden" name="no_note" value="0" />
					<input type="hidden" name="cn" value="Add special instructions to the seller:" />
				</Fragment>
			)}
			{ ! attributes.allow_special_instructions && (
				<input type="hidden" name="no_note" value="1" />
			)}

			{ attributes.default_tax_rate && (
				<input type="hidden" name="tax_rate" value={ attributes.default_tax_rate } />
			)}

			{ attributes.item_name && (
				<input type="hidden" name="item_name" value={ attributes.item_name } />
			)}

			{ attributes.item_id && (
				<input type="hidden" name="item_number" value={ attributes.item_id } />
			)}

			{ attributes.ask_for_address && (
				<input type="hidden" name="no_shipping" value="2" />
			)}
			{ ! attributes.ask_for_address && (
				<input type="hidden" name="no_shipping" value="1" />
			)}

			{ attributes.default_success_page && (
				<input type="hidden" name="return" value={ attributes.default_success_page } />
			)}
			{ attributes.default_cancel_page && (
				<input type="hidden" name="cancel_return" value={ attributes.default_cancel_page } />
			)}
			{ attributes.shipping_amount && (
				<input type="hidden" name="shipping" value={ attributes.shipping_amount } />
			)}

			{ submitButton }
		</form>
	);
}
