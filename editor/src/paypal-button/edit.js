
import {
	useBlockProps,
} from '@wordpress/block-editor';

import classnames from 'classnames';

import {
	InspectorControls,
	InspectorAdvancedControls,
	RichText,
} from '@wordpress/editor';

import {
	PanelBody,
	SelectControl,
	TextControl,
	ToggleControl,
	RadioControl,
} from '@wordpress/components';

import {
	Fragment,
} from '@wordpress/element';

import {
	getButtonImage,
	makePayLink,
} from './lib';


export default function edit(props){

	const blockProps = useBlockProps({});

	const { attributes, isSelected, setAttributes } = props;

	if( attributes.merchant_id == '' ) {

		window.requestAnimationFrame( () => {
			setAttributes({
				tax_rate: jakeparis.paypal.default_tax_rate,
				success_page: jakeparis.paypal.default_success_page,
				cancel_page: jakeparis.paypal.default_cancel_page,
				merchant_id: jakeparis.paypal.merchant_id,
			});
		});
	}

	const payLink = (attributes.transport_type === 'link') ? makePayLink(attributes) : false;
	const payLinkPreview = payLink ? (
		<p style={{padding:".4em 0 2em"}}>
			<a href={ payLink } target="_blank">Direct Link to Payment</a>
			<br />
			<i>Can be used in emails, etc.</i>
		</p>
	) : '';

	const settingsSidebar = (
		<Fragment>
			<InspectorControls>

				<PanelBody title="Basic Settings">
					<TextControl
						label="Shipping Amount (optional)"
						value={ attributes.shipping_amount }
						onChange={ shipping_amount => setAttributes({shipping_amount}) }
					/>
					<ToggleControl
						label="Use custom button"
						checked={ (attributes.button_type === 'custom') }
						onChange={ use_custom => setAttributes({
							button_type: use_custom ? 'custom' : 'basic'
						}) }
					/>
				</PanelBody>

				{ attributes.button_type == 'basic' && (
					<PanelBody title="Display Settings">
						<SelectControl
							label="Button Size"
							value={ attributes.button_size }
							options={[
								{ value: "small",  label: "Small" },
								{ 
									value: "medium", 
									label: "Medium", 
									disabled: !attributes.include_logo, 
								},
								{ value: "large",  label: "Large" },
							]}
							onChange={ button_size => setAttributes({button_size}) }
							disabled={ attributes.display_cards }
						/>

						{ ( !attributes.include_logo && attributes.button_type != 'custom') && (
							<SelectControl
								label="Button Text"
								value={ attributes.button_text }
								options={[
									{ value: 'Buy Now', label: "Buy Now" },
									{ value: 'Pay Now', label: "Pay Now" },
								]}
								onChange={ button_text => setAttributes({button_text}) }
							/>
						)}

						{ ( !attributes.include_logo && attributes.button_type === 'custom') && (
							<TextControl
								label="Button Text"
								value={ attributes.button_text }
								onChange={ button_text => setAttributes({button_text}) }
							/>
						)}

						{ ! attributes.include_logo && (
							<ToggleControl
								label="Display credit card logos"
								checked={ attributes.display_cards }
								onChange={ display_cards => setAttributes({display_cards}) }
							/>
						)}

						{ ! attributes.display_cards && (
							<ToggleControl
								label="Include PayPal logo"
								checked={ attributes.include_logo }
								onChange={ include_logo => setAttributes({include_logo}) }
								disabled={ attributes.display_cards }
							/>
						)}


					</PanelBody>
				)}
				{ attributes.button_type==='custom' &&(
					<PanelBody title="Display Settings">
						<ToggleControl
							label="Include PayPal logo"
							checked={ attributes.include_logo }
							onChange={ include_logo => setAttributes({include_logo}) }
							disabled={ attributes.display_cards }
						/>
					</PanelBody>
				)}


			</InspectorControls>
			<InspectorAdvancedControls>
					
					<ToggleControl
						label="Require shipping address from customer"
						checked={ attributes.ask_for_address }
						onChange={ ask_for_address => setAttributes({ask_for_address}) }
					/>
					
					<ToggleControl
						label="Allow special instructions from customer"
						checked={ attributes.allow_special_instructions }
						onChange={ allow_special_instructions => setAttributes({allow_special_instructions}) }
					/>

					<div>
						<RadioControl
							label="Type of transport"
							selected={ attributes.transport_type }
							options={[
								{ label: "Form", value: "form" },
								{ label: "Link", value: "link" },
							]}
							onChange={ transport_type => setAttributes({transport_type}) }
						/>
						
						{ payLinkPreview }
					</div>

			</InspectorAdvancedControls>
		</Fragment>
	);

	const inlineControls = (
		<div style={{ display:"flex", gap: ".7em" }}>
			<TextControl
				label="Dollar Amount"
				value={ attributes.amount }
				onChange={ amount => setAttributes({amount}) }
			/>
			<TextControl
				label="Item Name"
				help="(optional) name of what is being purchased"
				value={ attributes.item_name }
				onChange={ item_name => setAttributes({item_name}) }
			/>
			<TextControl
				label="Item ID"
				help="(optional) item id"
				value={ attributes.item_id }
				onChange={ item_id => setAttributes({item_id}) }
			/>
		</div>
	);

	const imageUrl = getButtonImage( attributes );

	const customButtonClasses = classnames({
		'jp-custom-paypal-button': true,
		'wp-block-button__link': true,
		'_with-logo': (attributes.include_logo===true),
	});

	const customButton = (attributes.button_type==='custom') ? (
		<div className="wp-block-button">
			<RichText
				tagName="a"
				className={ customButtonClasses }
				value={ attributes.button_text }
				onChange={ button_text => setAttributes({button_text}) }
				allowedFormats={[
					'core/bold','core/italic',
				]}
			/>
		</div>
	) : '';

	return (
		<div {...blockProps}>

			{ isSelected && inlineControls }

			{ attributes.button_type === 'basic' && (
				<input type="image" src={ imageUrl } name="submit" alt="Pay with Paypal" />
			)}
			
			{ customButton }

			{ settingsSidebar }
		</div>
	);

}
